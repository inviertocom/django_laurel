from django import forms
from .models import Client

class ClientForm(forms.ModelForm):

    class Meta:
        model = Client
        fields = ['dpi', 'nit', 'nombre', 'apellidos', 'telefonoCelular']
        widget = {
            'dpi': forms.TextInput(attrs={'class':'form-control', 'placeholder':'DPI'}),
            'nit': forms.TextInput(attrs={'class':'form-control', 'placeholder':'NIT'}),
            'nombre': forms.TextInput(attrs={'class':'form-control', 'placeholder':'Nombre'}),
            'apellido': forms.TextInput(attrs={'class':'form-control', 'placeholder':'Apellidos'}),
            'telefonoCelular': forms.TextInput(attrs={'class':'form-control', 'placeholder':'Telefono Celular'}),
        }

        # labels = {
        #     'dpi': '',
        #     'nit': '',
        #     'nombre': '',
        #     'apellidos': '',
        #     'telefonoCelular': '',
        # }


class clientCompleteForm(forms.ModelForm):
    class Meta:
        model = Client
        fields = ['dpi', 'nit', 'nombre', 'apellidos', 'telefonoCelular', 
            'telefonoDomicilio', 'telefonoReferencia', 'fechaNacimiento', 'lugarNacimiento', 'direccion'
        ]
        widget = {
            'dpi': forms.TextInput(attrs={'class':'form-control'}),
            'nit': forms.Textarea(attrs={'class':'form-control'}),
            'nombre': forms.TextInput(attrs={'class':'form-control'}),
            'apellido': forms.Textarea(attrs={'class':'form-control'}),
            'telefonoCelular': forms.TextInput(attrs={'class':'form-control'}),
            'telefonoDomicilio': forms.TextInput(attrs={'class':'form-control'}), 
            'telefonoReferencia': forms.TextInput(attrs={'class':'form-control'}),
            'fechaNacimiento': forms.TextInput(attrs={'class':'form-control'}),
            'lugarNacimiento': forms.TextInput(attrs={'class':'form-control'}),
            'direccion': forms.TextInput(attrs={'class':'form-control'}),
        }