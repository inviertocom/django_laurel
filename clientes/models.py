from django.db import models

# Create your models here.
class Client(models.Model):
    id = models.IntegerField(unique=True, primary_key=True)
    dpi = models.CharField(max_length=30, null=True)
    nit = models.CharField(max_length=10, null=True)
    nombre = models.CharField(max_length=40, null=True)
    apellidos = models.CharField(max_length=40, null=True)
    telefonoDomicilio = models.CharField(max_length=10, null=True)
    telefonoCelular = models.CharField(max_length=10, null=True)
    telefonoReferencia = models.CharField(max_length=255, null=True)
    fechaNacimiento = models.CharField(max_length=255, null=True)
    lugarNacimiento = models.CharField(max_length=10, null=True)
    direccion = models.CharField(max_length=10, null=True)
    created = models.DateTimeField(auto_now_add=True, verbose_name="Fecha de creación")
    updated = models.DateTimeField(auto_now=True, verbose_name="Fecha de edición")

    class Meta:
        verbose_name = "Cliente"
        verbose_name_plural = "Clientes"
        ordering = ['apellidos', 'nombre']

    def __str__(self):
        return self.title
