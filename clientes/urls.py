from django.urls import path
from .views import ClientsListView, ClientsCreate, ClientsDelete, ClientsDetailView, ClientsUpdate

clients_patterns = ([
    path('', ClientsListView.as_view(), name='clients'),
    path('<int:pk>/', ClientsDetailView.as_view(), name='clientd'),
    path('create/', ClientsCreate.as_view(), name='create'),
    path('update/<int:pk>/', ClientsUpdate.as_view(), name='update'),
    path('delete/<int:pk>/', ClientsDelete.as_view(), name='delete'),
], 'clientes')