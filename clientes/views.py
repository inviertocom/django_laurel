from django.shortcuts import render
from .models import Client
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy

from .forms import ClientForm

# Create your views here.

# Create your views here.
class ClientsListView(ListView):
    model = Client

class ClientsDetailView(DetailView):
    model = Client

class ClientsCreate(CreateView):
    model = Client
    form_class = ClientForm
    success_url = reverse_lazy('clientes:clients')

class ClientsUpdate(UpdateView):
    model = Client
    fields = ['dpi', 'nit', 'nombre', 'apellidos', 'telefonoDomicilio', 'telefonoCelular', 'fechaNacimiento', 'lugarNacimiento', 'direccion']
    template_name_suffix = '_update_form'
    def get_success_url(self):
        return reverse_lazy('clientes:update', args=[self.object.id]) + '?ok'

class ClientsDelete(DeleteView):
    model = Client
    success_url = reverse_lazy('clientes:clients')