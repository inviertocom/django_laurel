from django.urls import path
from .views import CobrosListView

cobros_patterns = ([
    path('', CobrosListView.as_view(), name='cobros'),
    # path('<int:pk>/<slug:slug>/', , name='cobro'),
    # path('create/', , name='create'),
    # path('update/<int:pk>/', , name='update'),
    # path('delete/<int:pk>/', , name='delete'),
], 'cobros')