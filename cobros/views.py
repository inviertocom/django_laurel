from django.shortcuts import render
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy

from .models import Cobros
from .forms import CobrosForm

# Create your views here.

# Create your views here.
class CobrosListView(ListView):
    model = Cobros
    template_name = "Cobros/cobro_list.html" 

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name, {'title': 'Cobros'})

class CobrosDetailView(DetailView):
    model = Cobros

class CobrosCreate(CreateView):
    model = Cobros
    form_class = CobrosForm
    success_url = reverse_lazy('Cobros:cobro')

class CobrosUpdate(UpdateView):
    model = Cobros
    fields = []
    template_name_suffix = '_update_form'
    def get_success_url(self):
        return reverse_lazy('cobros:update', args=[self.object.id]) + '?ok'

class PageDelete(DeleteView):
    model = Cobros
    success_url = reverse_lazy('cobros:cobros')