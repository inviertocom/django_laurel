from django.contrib import admin
from .models import Prestamo
# Register your models here.

class PrestamoAdmin(admin.ModelAdmin):
    list_display = ('cantidad', 'tasa')

admin.site.register(Prestamo, PrestamoAdmin)