from django import forms
from .models import Prestamo

class PrestamoForm(forms.ModelForm):

    class Meta:
        model = Prestamo
        fields = ['cantidad', 'tasa', 'pagos']
        widget = {
            'cantidad': forms.TextInput(attrs={'class':'form-control', 'placeholder':'Cantidad Prestamo'}),
            'tasa': forms.TextInput(attrs={'class':'form-control', 'placeholder':'Tasa Interes'}),
            'pagos': forms.TextInput(attrs={'class':'form-control', 'placeholder':'Cantidad Pagos'}),
        }

class PrestamoCompleteForm(forms.ModelForm):
    class Meta:
        model = Prestamo
        fields = []
        # fields = ['dpi', 'nit', 'nombre', 'apellidos', 'telefonoCelular', 
        #     'telefonoDomicilio', 'telefonoReferencia', 'fechaNacimiento', 'lugarNacimiento', 'direccion'
        # ]
        # widget = {
        #     'dpi': forms.TextInput(attrs={'class':'form-control'}),
        #     'nit': forms.Textarea(attrs={'class':'form-control'}),
        #     'nombre': forms.TextInput(attrs={'class':'form-control'}),
        #     'apellido': forms.Textarea(attrs={'class':'form-control'}),
        #     'telefonoCelular': forms.TextInput(attrs={'class':'form-control'}),
        #     'telefonoDomicilio': forms.TextInput(attrs={'class':'form-control'}), 
        #     'telefonoReferencia': forms.TextInput(attrs={'class':'form-control'}),
        #     'fechaNacimiento': forms.TextInput(attrs={'class':'form-control'}),
        #     'lugarNacimiento': forms.TextInput(attrs={'class':'form-control'}),
        #     'direccion': forms.TextInput(attrs={'class':'form-control'}),
        # }