from django.db import models

# Create your models here.
class Prestamo(models.Model):
    id = models.IntegerField(unique=True, primary_key=True)
    cantidad = models.IntegerField(verbose_name='Cantidad Prestamo', null=True, default=0)
    tasa = models.SmallIntegerField(verbose_name='Tasa Interes', null=True, default=0)
    fechaPago = models.CharField(max_length=2, verbose_name='Fecha de Pago', null=True, default=0)
    estado = models.SmallIntegerField(verbose_name='Estado del Prestamo', null=True, default=0)
    colocadorID = models.IntegerField(verbose_name='colocadorID', null=True, default=0)
    autorizadoID = models.IntegerField(verbose_name='autorizadoID', null=True, default=0)
    clienteID = models.IntegerField(verbose_name='clienteID', null=True, default=0)
    revisadoID = models.IntegerField(verbose_name='revisadoID', null=True, default=0)
    moroso = models.IntegerField(verbose_name='Meses Moroso', null=True, default=0)
    exonerado = models.BooleanField(verbose_name='Exonerado', null=True, default=False)
    pagos = models.IntegerField(verbose_name='Cantidad de Pagos', null=True, default=0)
    restante = models.IntegerField(verbose_name='Cantidad Restante', null=True, default=0)
    created = models.DateTimeField(auto_now_add=True, verbose_name="Fecha de creación")
    updated = models.DateTimeField(auto_now=True, verbose_name="Fecha de edición")

    class Meta:
        verbose_name = "Prestamo"
        verbose_name_plural = "Prestamos"
        ordering = ['created']

    def __str__(self):
        return self.title
