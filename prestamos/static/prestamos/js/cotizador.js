function cotizar(cantidad, pagos) {
    this.americano = [];
    this.frances = [];
    this.cotizado = true;
    if (this.cantidad >= 20000 && this.cantidad <= 30999) {
      this.interes = 5;
    }
    else if (this.cantidad >= 31000 && this.cantidad <= 69999) {
      this.interes = 4;
    }
    else if (this.cantidad >= 70000 && this.cantidad <= 100000) {
      this.interes = 3;
    }
    this.interes = (this.interes / 100);
    this.pagos = Number(this.pagos);
    this.cantidad = Number(this.cantidad);

    for (let index = 1; index < this.pagos; index++) {
      this.americano.push({'no': index, 'cantidad': (this.cantidad * this.interes)});
    }
    this.americano.push({'no': this.pagos, 'cantidad': (this.cantidad * this.interes) + this.cantidad});
    this.pmt = (this.cantidad * this.interes) / (1 - Math.pow(1 + this.interes, this.pagos));
    this.pmt = this.pmt * -1;
    this.pmt = this.pmt + (this.cantidad * this.interes);
    for (let index = 1; index <= this.pagos; index++) {
      this.frances.push({'no': index, 'cantidad': this.pmt});
    }
}