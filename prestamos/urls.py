from django.urls import path
from .views import PrestamoCreate, PrestamoDelete, PrestamoDetailView, PrestamoListView, PrestamoUpdate, PrestamoCotizador, Calcular, Avaluos

prestamos_patterns = ([
    path('bolsa/', PrestamoListView.as_view(), name='prestamos'),
    path('cotizador/', PrestamoCotizador, name='cotizador'),
    path('<int:pk>/', PrestamoDetailView.as_view(), name='prestamo'),
    path('create/', PrestamoCreate.as_view(), name='create'),
    path('update/<int:pk>/', PrestamoUpdate.as_view(), name='update'),
    path('delete/<int:pk>/', PrestamoDelete.as_view(), name='delete'),
    path('calcular/', Calcular, name='calcular'),
    path('avaluos/', Avaluos , name='avaluos'),

], 'prestamos')