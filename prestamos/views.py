from django.shortcuts import render
from .models import Prestamo
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy

from .forms import PrestamoForm
from .forms import PrestamoCompleteForm

class PrestamoListView(ListView):
    model = Prestamo

class PrestamoDetailView(DetailView):
    model = Prestamo

class PrestamoCreate(CreateView):
    model = Prestamo
    form_class = PrestamoForm
    success_url = reverse_lazy('prestamos:prestamos')

class PrestamoUpdate(UpdateView):
    model = Prestamo
    fields = []
    template_name_suffix = '_update_form'
    def get_success_url(self):
        return reverse_lazy('clientes:update', args=[self.object.id]) + '?ok'

class PrestamoDelete(DeleteView):
    model = Prestamo
    success_url = reverse_lazy('clientes:clients')

def PrestamoCotizador(request):
    cotizado = False
    return render(request, 'prestamos/cotizador.html', {'cotizado':cotizado})


def Avaluos(request):
    return render(request, 'prestamos/avaluos.html')

def Calcular(request):
    try:
        cantidad = int(request.POST.get('cantidad'))
        pagos = int(request.POST.get('pagos'))
    except:
        cantidad = 0
        pagos = 0
    americano = []
    frances = []
    cotizado = True
    if (cantidad >= 20000 and cantidad <= 30999):
        interes = 5
    elif (cantidad >= 31000 and cantidad <= 69999):
        interes = 4
    elif (cantidad >= 70000 and cantidad <= 100000):
        interes = 3

    interes = (interes / 100)

    for index in range(pagos - 1):
        americano.append({'no': index + 1, 'cantidad': round(cantidad * interes, 2)})

    americano.append({'no': pagos, 'cantidad': round((cantidad * interes) + cantidad,2)})
    pmt = (cantidad * interes) / (1 - pow(1 + interes, pagos))
    pmt = pmt * -1
    pmt = pmt + (cantidad * interes)

    for index in range(pagos):
        frances.append({'no': index + 1, 'cantidad': round(pmt, 2)})
    
    return render(request, 'prestamos/cotizador.html', {'cotizado':cotizado, 'americano': americano, 'frances': frances, 'interes': interes})
    
